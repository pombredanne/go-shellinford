package main

// $ go run main.go

import (
	"bitbucket.org/oov/go-shellinford/shellinford"
	"bufio"
	"fmt"
	"os"
	"time"
)

func main() {
	f, err := os.Open("anime.txt")
	if err != nil {
		panic(err)
	}

	defer f.Close()
	sc := bufio.NewScanner(f)

	fm := shellinford.NewFMIndex()
	st := time.Now()
	i := 0
	for sc.Scan() {
		fm.Add(sc.Bytes())
		i++
	}
	fmt.Printf("%d Entry added. (%fsecs)\n", i, time.Now().Sub(st).Seconds())

	fmt.Printf("Creating indexes...")
	st = time.Now()
	fm.Build(0, 1)
	fmt.Printf("done. (%fsecs)\n", time.Now().Sub(st).Seconds())

	w := "探偵"
	st = time.Now()
	found := fm.Search([]byte(w))
	fmt.Printf("Keyword \"%s\" found %d entries. (%fsecs)\n", w, len(found), time.Now().Sub(st).Seconds())
	fmt.Println()
	for k, v := range found {
		fmt.Println(string(fm.Document(k)), v, "Hit(s)")
	}
}
