// Copyright 2013 The Shellinford-Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package bit

import (
	"encoding/binary"
	"io"
)

type Vector struct {
	v       []uint64
	r       []int
	length  int
	length1 int
}

func rank64(x uint64) int {
	x = ((x & 0xaaaaaaaaaaaaaaaa) >> 1) + (x & 0x5555555555555555)
	x = ((x & 0xcccccccccccccccc) >> 2) + (x & 0x3333333333333333)
	x = ((x & 0xf0f0f0f0f0f0f0f0) >> 4) + (x & 0x0f0f0f0f0f0f0f0f)
	x = ((x & 0xff00ff00ff00ff00) >> 8) + (x & 0x00ff00ff00ff00ff)
	x = ((x & 0xffff0000ffff0000) >> 16) + (x & 0x0000ffff0000ffff)
	x = ((x & 0xffffffff00000000) >> 32) + (x & 0x00000000ffffffff)
	return int(x)
}

func select64(x, i uint64, b bool) int {
	if !b {
		x = ^x
	}
	x1 := ((x & 0xaaaaaaaaaaaaaaaa) >> 0x01) + (x & 0x5555555555555555)
	x2 := ((x1 & 0xcccccccccccccccc) >> 0x02) + (x1 & 0x3333333333333333)
	x3 := ((x2 & 0xf0f0f0f0f0f0f0f0) >> 0x04) + (x2 & 0x0f0f0f0f0f0f0f0f)
	x4 := ((x3 & 0xff00ff00ff00ff00) >> 0x08) + (x3 & 0x00ff00ff00ff00ff)
	x5 := ((x4 & 0xffff0000ffff0000) >> 0x10) + (x4 & 0x0000ffff0000ffff)

	i++
	var pos uint
	v5 := x5 & 0xffffffff
	if i > v5 {
		i -= v5
		pos += 0x20
	}
	v4 := (x4 >> pos) & 0x0000ffff
	if i > v4 {
		i -= v4
		pos += 0x10
	}
	v3 := (x3 >> pos) & 0x000000ff
	if i > v3 {
		i -= v3
		pos += 0x8
	}
	v2 := (x2 >> pos) & 0x0000000f
	if i > v2 {
		i -= v2
		pos += 0x4
	}
	v1 := (x1 >> pos) & 0x00000003
	if i > v1 {
		i -= v1
		pos += 0x2
	}
	v0 := (x >> pos) & 0x00000001
	if i > v0 {
		i -= v0
		pos += 0x1
	}
	return int(pos)
}

func NewVector() *Vector {
	return &Vector{}
}

func OpenVector(r io.Reader) (*Vector, error) {
	v := &Vector{}

	var err error
	b := make([]byte, 8)

	if _, err = r.Read(b); err != nil {
		return nil, err
	}
	v.length = int(binary.LittleEndian.Uint64(b))

	if _, err = r.Read(b); err != nil {
		return nil, err
	}
	v.length1 = int(binary.LittleEndian.Uint64(b))

	if _, err = r.Read(b); err != nil {
		return nil, err
	}
	v.v = make([]uint64, binary.LittleEndian.Uint64(b))
	for i := range v.v {
		if _, err = r.Read(b); err != nil {
			return nil, err
		}
		v.v[i] = uint64(binary.LittleEndian.Uint64(b))
	}

	if _, err = r.Read(b); err != nil {
		return nil, err
	}
	v.r = make([]int, binary.LittleEndian.Uint64(b))
	for i := range v.r {
		if _, err = r.Read(b); err != nil {
			return nil, err
		}
		v.r[i] = int(binary.LittleEndian.Uint64(b))
	}
	return v, nil
}

func (v *Vector) Reset() {
	v.v = v.v[:0]
	v.r = v.r[:0]
	v.length = 0
	v.length1 = 0
}

func (v *Vector) Get(i int) bool {
	/*
		if i >= v.length {
			panic(fmt.Sprintf("bit: Vector.Get: i:%d is out of range(len:%d)", i, v.length))
		}
	*/
	m := uint64(1 << uint(i&0x3f)) // 1 << (i % smallBlockSize)
	return (v.v[i>>6] & m) != 0
}

func (v *Vector) Set(i int, b bool) {
	q := i >> 6
	m := uint64(1 << uint(i&0x3f)) // 1 << (i % smallBlockSize)
	if i >= v.length {
		v.length = i + 1
		for ln := len(v.v); q >= ln; ln++ {
			v.v = append(v.v, 0)
		}
	}
	if b {
		v.v[q] |= m
	} else {
		v.v[q] &^= m
	}
}

func (v *Vector) Build() {
	v.r = v.r[:0]
	v.length1 = 0
	for i := range v.v {
		if (i & 7) == 0 { // i%blockRate == 0
			v.r = append(v.r, v.RankLen(true))
		}
		v.length1 += rank64(v.v[i])
	}
}

func (v *Vector) Len() int {
	return v.length
}

func (v *Vector) RankLen(b bool) int {
	if b {
		return v.length1
	}
	return v.length - v.length1
}

func (v *Vector) Rank(i int, b bool) int {
	/*
		if i > v.length {
			panic(fmt.Sprintf("bit: Vector.Rank: i:%d is out of range(len:%d)", i, v.length))
		}
	*/
	if i == 0 {
		return 0
	}
	i--
	qLarge := i >> 9
	qSmall := i >> 6
	if b {
		rank := v.r[qLarge]
		for j := qLarge << 3; j < qSmall; j++ {
			rank += rank64(v.v[j])
		}
		rank += rank64(v.v[qSmall] << uint(0x3f-(i&0x3f)))
		return rank
	} else {
		rank := (qLarge << 9) - v.r[qLarge]
		for j := qLarge << 3; j < qSmall; j++ {
			rank += rank64(^v.v[j])
		}
		rank += rank64((^v.v[qSmall]) << uint(0x3f-(i&0x3f)))
		return rank
	}
}

func (v *Vector) Select(i int, b bool) int {
	/*
		if i >= v.RankLen(b) {
			panic(fmt.Sprintf("bit: Vector.Select: i:%d is out of range(len:%d)", i, v.RankLen(b)))
		}
	*/
	var pivot, rank int
	left, right := 0, len(v.r)
	for left < right {
		pivot = (left + right) >> 1
		rank = v.r[pivot]
		if !b {
			rank = (pivot << 9) - rank
		}
		if i < rank {
			right = pivot
		} else {
			left = pivot + 1
		}
	}
	right--

	if b {
		i -= v.r[right]
	} else {
		i -= (right << 9) - v.r[right]
	}
	j := right << 3
	for {
		if b {
			rank = rank64(v.v[j])
		} else {
			rank = rank64(^v.v[j])
		}
		if i < rank {
			break
		}
		j++
		i -= rank
	}
	return (j << 6) + select64(v.v[j], uint64(i), b)
}

func (v *Vector) Write(w io.Writer) error {
	var err error
	b := make([]byte, 8)

	binary.LittleEndian.PutUint64(b, uint64(v.length))
	if _, err = w.Write(b); err != nil {
		return err
	}

	binary.LittleEndian.PutUint64(b, uint64(v.length1))
	if _, err = w.Write(b); err != nil {
		return err
	}

	binary.LittleEndian.PutUint64(b, uint64(len(v.v)))
	if _, err = w.Write(b); err != nil {
		return err
	}
	for _, x := range v.v {
		binary.LittleEndian.PutUint64(b, uint64(x))
		if _, err = w.Write(b); err != nil {
			return err
		}
	}

	binary.LittleEndian.PutUint64(b, uint64(len(v.r)))
	if _, err = w.Write(b); err != nil {
		return err
	}
	for _, x := range v.r {
		binary.LittleEndian.PutUint64(b, uint64(x))
		if _, err = w.Write(b); err != nil {
			return err
		}
	}
	return nil
}
