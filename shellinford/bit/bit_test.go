// Copyright 2013 The Shellinford-Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package bit

import (
	"bytes"
	"testing"
)

func BenchmarkGetSet(b *testing.B) {
	bv := NewVector()
	for i := 0; i < b.N; i++ {
		bv.Set(i, i&1 != 0)
		bv.Get(i)
	}
}
func BenchmarkBuildRank(b *testing.B) {
	bv := NewVector()
	for i := 0; i < b.N; i++ {
		bv.Set(i, i&1 != 0)
		bv.Get(i)
		if i%100 == 0 {
			bv.Build()
			bv.Rank(i/2, true)
			bv.Rank(i/2, false)
		}
	}
}

func setup() (values []int, bv0 *Vector, bv1 *Vector) {
	values = []int{0, 511, 512, 1000, 2000, 3000}
	bv0 = NewVector()
	bv1 = NewVector()
	for i := 0; i <= values[len(values)-1]; i++ {
		bv0.Set(i, true)
	}
	for _, v := range values {
		bv1.Set(v, true)
		bv0.Set(v, false)
	}
	bv0.Build()
	bv1.Build()
	return
}

func TestLen(t *testing.T) {
	values, bv0, bv1 := setup()

	if bv1.Len() != values[len(values)-1]+1 {
		t.Logf("bv1.Len() = %d / Success = %d", bv1.Len(), values[len(values)-1]+1)
		t.Fail()
	}
	if bv1.RankLen(true) != len(values) {
		t.Logf("bv1.RankLen(true) = %d / Success = %d", bv1.RankLen(true), len(values))
		t.Fail()
	}
	if bv0.Len() != values[len(values)-1]+1 {
		t.Logf("bv0.Len() = %d / Success = %d", bv0.Len(), values[len(values)-1]+1)
		t.Fail()
	}
	if bv0.RankLen(false) != len(values) {
		t.Logf("bv0.RankLen(false) = %d / Success = %d", bv0.RankLen(false), len(values))
		t.Fail()
	}
}

func TestGet(t *testing.T) {
	values, bv0, bv1 := setup()

	for _, v := range values {
		if bv1.Get(v) != true {
			t.Logf("bv1.Get(v) = %t / Success = %t", bv1.Get(v), true)
			t.Fail()
		}
		if bv0.Get(v) != false {
			t.Logf("bv0.Get(v) = %t / Success = %t", bv0.Get(v), true)
			t.Fail()
		}
	}
}

func TestRank(t *testing.T) {
	values, bv0, bv1 := setup()

	var ct int
	for _, v := range values {
		if bv1.Rank(v, true) != ct {
			t.Logf("bv1.Rank(%d, true) = %d / Success = %d", v, bv1.Rank(v, true), ct)
			t.Fail()
		}
		if bv0.Rank(v, false) != ct {
			t.Logf("bv0.Rank(%d, false) = %d / Success = %d", v, bv0.Rank(v, false), ct)
			t.Fail()
		}
		ct++
	}
}

func TestSelect(t *testing.T) {
	values, bv0, bv1 := setup()

	var ct int
	for _, v := range values {
		if bv1.Select(ct, true) != v {
			t.Logf("bv1.Select(%d, true) = %d / Success = %d", ct, bv1.Select(ct, true), v)
			t.Fail()
		}
		if bv0.Select(ct, false) != v {
			t.Logf("bv0.Select(%d, false) = %d / Success = %d", ct, bv0.Select(ct, false), v)
			t.Fail()
		}
		ct++
	}
}

func TestGetBoundary(t *testing.T) {
	t.Skip("bounds checking is disabled")
	_, bv0, bv1 := setup()

	func() {
		defer func() {
			if recover() == nil {
				t.Fail()
			}
		}()
		bv1.Get(bv1.Len())
	}()
	func() {
		defer func() {
			if recover() == nil {
				t.Fail()
			}
		}()
		bv0.Get(bv0.Len())
	}()
}

func TestRankBoundary(t *testing.T) {
	t.Skip("bounds checking is disabled")
	_, bv0, bv1 := setup()

	func() {
		defer func() {
			if recover() == nil {
				t.Fail()
			}
		}()
		bv1.Rank(bv1.Len()+1, true)
	}()
	func() {
		defer func() {
			if recover() == nil {
				t.Fail()
			}
		}()
		bv1.Rank(bv1.Len()+1, false)
	}()
	func() {
		defer func() {
			if recover() == nil {
				t.Fail()
			}
		}()
		bv0.Rank(bv0.Len()+1, false)
	}()
	func() {
		defer func() {
			if recover() == nil {
				t.Fail()
			}
		}()
		bv0.Rank(bv0.Len()+1, false)
	}()
}

func TestSelectBoundary(t *testing.T) {
	t.Skip("bounds checking is disabled")
	_, bv0, bv1 := setup()

	func() {
		defer func() {
			if recover() == nil {
				t.Fail()
			}
		}()
		bv1.Select(bv1.RankLen(true), true)
	}()
	func() {
		defer func() {
			if recover() == nil {
				t.Fail()
			}
		}()
		bv1.Select(bv1.RankLen(false), false)
	}()
	func() {
		defer func() {
			if recover() == nil {
				t.Fail()
			}
		}()
		bv0.Select(bv0.RankLen(true), true)
	}()
	func() {
		defer func() {
			if recover() == nil {
				t.Fail()
			}
		}()
		bv0.Select(bv0.RankLen(false), false)
	}()
}

func TestWriteRead(t *testing.T) {
	_, bv0, _ := setup()

	bf := bytes.NewBufferString("")
	if err := bv0.Write(bf); err != nil {
		t.Error(err)
	}
	bv, err := OpenVector(bytes.NewReader(bf.Bytes()))
	if err != nil {
		t.Error(err)
	}

	if bv0.length != bv.length {
		t.Fail()
	}

	if bv0.length1 != bv.length1 {
		t.Fail()
	}

	if len(bv0.v) != len(bv.v) {
		t.Fail()
	}
	for i, bv0v := range bv0.v {
		if bv0v != bv.v[i] {
			t.Fail()
		}
	}

	if len(bv0.r) != len(bv.r) {
		t.Fail()
	}
	for i, bv0v := range bv0.r {
		if bv0v != bv.r[i] {
			t.Fail()
		}
	}
}
