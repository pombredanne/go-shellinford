package bwt

import (
	"testing"
)

func TestBS(t *testing.T) {
	before := []byte("hello")
	after, _ := Do(before)

	// hello
	// elloh
	// llohe
	// lohel
	// ohell
	// -----
	// elloh
	// hello
	// llohe
	// lohel
	// ohell
	// -----
	// hoell
	if string(after) != "hoell" {
		t.Fail()
	}
}
