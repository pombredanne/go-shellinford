// Copyright 2013 The Shellinford-Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package bwt

import (
	"bitbucket.org/oov/go-shellinford/shellinford/mkqsort"
)

type bwt struct {
	idx []int
	buf []byte
	ln  int
}

func (bwt *bwt) Len() int      { return bwt.ln }
func (bwt *bwt) DataLen() int  { return bwt.ln }
func (bwt *bwt) Swap(i, j int) { bwt.idx[i], bwt.idx[j] = bwt.idx[j], bwt.idx[i] }
func (bwt *bwt) CompareAt(i, j, idx int) mkqsort.CompareResult {
	a := bwt.Get(i, idx)
	b := bwt.Get(j, idx)
	switch {
	case a < b:
		return mkqsort.CompareResultLess
	case a > b:
		return mkqsort.CompareResultGreater
	}
	return mkqsort.CompareResultEqual
}

func (bwt *bwt) Get(idx, pos int) byte {
	pos += bwt.idx[idx]
	if pos >= bwt.ln {
		pos -= bwt.ln
	}
	return bwt.buf[pos]
}

func Do(data []byte) (sorted []byte, head int) {
	ln := len(data)
	b := &bwt{
		idx: make([]int, ln),
		buf: data,
		ln:  ln,
	}
	for i := 0; i < ln; i++ {
		b.idx[i] = i
	}

	mkqsort.Sort(b)
	sorted = make([]byte, ln)
	for i := 0; i < ln; i++ {
		sorted[i] = b.Get(i, ln-1)
		if b.idx[i] == 0 {
			head = i
		}
	}
	return
}
