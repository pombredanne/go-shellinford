// Copyright 2013 The Shellinford-Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package wavelet

import (
	"bitbucket.org/oov/go-shellinford/shellinford/bit"
	"encoding/binary"
	"fmt"
	"io"
)

type depth struct {
	bv  *bit.Vector
	sep int
}

type Matrix struct {
	d      [8]*depth
	p      [256]int
	length int
}

func newMatrix() *Matrix {
	m := &Matrix{}
	for i := range m.d {
		m.d[i] = &depth{
			bv:  bit.NewVector(),
			sep: 0,
		}
	}
	return m
}
func NewMatrix(bytes []byte) *Matrix {
	m := newMatrix()

	ln := len(bytes)
	m.length = ln
	buf := make([]byte, ln)
	buf2 := make([]byte, ln)
	copy(buf, bytes)

	keybit := byte(1 << 7)
	for _, d := range m.d {
		lpos, rpos := 0, ln-1
		d.bv.Set(rpos, false)
		for i, b := range buf {
			if (b & keybit) != 0 {
				d.bv.Set(i, true)
				buf2[rpos] = b
				rpos--
			} else {
				buf2[lpos] = b
				lpos++
			}
		}
		d.sep = lpos
		d.bv.Build()

		rpos = ln - 1
		for lpos < rpos {
			buf2[lpos], buf2[rpos] = buf2[rpos], buf2[lpos]
			lpos++
			rpos--
		}
		buf, buf2 = buf2, buf
		keybit >>= 1
	}

	bp := m.p[:]
	for idx := range bp {
		bp[idx] = -1
	}
	if ln == 0 {
		return m
	}

	bc := buf[0] + 1
	for idx, b := range buf {
		if b == bc {
			continue
		}
		bp[b] = idx
		bc = b
	}
	return m
}

func (m *Matrix) Len() int {
	return m.length
}

func (m *Matrix) RankLen(c byte) int {
	return m.Rank(m.length, c)
}

func (m *Matrix) Get(i int) byte {
	/*
		if i >= m.length {
			panic(fmt.Sprintf("wavelet: Matrix.Get: i:%d is out of range(len:%d)", i, m.length))
		}
	*/
	var value byte
	for _, d := range m.d {
		bit := d.bv.Get(i)
		i = d.bv.Rank(i, bit)
		value <<= 1
		if bit {
			i += d.sep
			value += 1
		}
	}
	return value
}

func (m *Matrix) Rank(i int, c byte) int {
	/*
		if i > m.length {
			panic(fmt.Sprintf("wavelet: Matrix.Rank: i:%d is out of range(len:%d)", i, m.length))
		}
	*/
	if i == 0 {
		return 0
	}

	begin := m.p[c]
	if begin == -1 {
		return 0
	}

	var bit bool
	keybit := byte(1 << 7)
	for _, d := range m.d {
		bit = c&keybit != 0
		i = d.bv.Rank(i, bit)
		if bit {
			i += d.sep
		}
		keybit >>= 1
	}

	return i - begin
}

func (m *Matrix) RankLessThan(i int, c byte) int {
	/*
		if i > m.length {
			panic(fmt.Sprintf("wavelet: Matrix.RankLessThan: i:%d is out of range(len:%d)", i, m.length))
		}
	*/
	if i == 0 {
		return 0
	}
	var begin, end, rlt int = 0, i, 0
	keybit := byte(1 << 7)
	for _, d := range m.d {
		rank0_begin := d.bv.Rank(begin, false)
		rank0_end := d.bv.Rank(end, false)
		if c&keybit != 0 {
			rlt += rank0_end - rank0_begin
			begin += d.sep - rank0_begin
			end += d.sep - rank0_end
		} else {
			begin = rank0_begin
			end = rank0_end
		}
		keybit >>= 1
	}
	return rlt
}

func (m *Matrix) Select(i int, c byte) int {
	/*
		if i >= m.RankLen(c) {
			panic(fmt.Sprintf("wavelet: Matrix.Select: i:%d is out of range(len:%d)", i, m.RankLen(c)))
		}
	*/
	for left, right := uint64(0), uint64(m.length); left < right; {
		pivot := (left + right) >> 1
		value := m.Rank(int(pivot), c)
		switch {
		case i < value:
			right = pivot
		case i > value:
			left = pivot + 1
		default:
			for pivot > 0 && m.Get(int(pivot)) != c {
				pivot++
			}
			return int(pivot)
		}
	}
	panic(fmt.Sprintf("wavelet: Matrix.Select: ??? i:%d c:%d", i, c))
}

func OpenMatrix(r io.Reader) (*Matrix, error) {
	m := newMatrix()

	var err error
	b := make([]byte, 8)

	if _, err = r.Read(b); err != nil {
		return nil, err
	}
	m.length = int(binary.LittleEndian.Uint64(b))

	for _, d := range m.d {
		if d.bv, err = bit.OpenVector(r); err != nil {
			return nil, err
		}
	}
	for _, d := range m.d {
		if _, err = r.Read(b); err != nil {
			return nil, err
		}
		d.sep = int(binary.LittleEndian.Uint64(b))
	}

	if _, err = r.Read(b); err != nil {
		return nil, err
	}
	for i := range m.p {
		if _, err = r.Read(b); err != nil {
			return nil, err
		}
		if _, err = r.Read(b); err != nil {
			return nil, err
		}
		m.p[i] = int(binary.LittleEndian.Uint64(b))
	}

	return m, nil
}

func (m *Matrix) Write(w io.Writer) error {
	var err error
	b := make([]byte, 8)

	binary.LittleEndian.PutUint64(b, uint64(m.length))
	if _, err = w.Write(b); err != nil {
		return err
	}

	for _, d := range m.d {
		if err = d.bv.Write(w); err != nil {
			return err
		}
	}

	for _, d := range m.d {
		binary.LittleEndian.PutUint64(b, uint64(d.sep))
		if _, err = w.Write(b); err != nil {
			return err
		}
	}

	binary.LittleEndian.PutUint64(b, 256)
	if _, err = w.Write(b); err != nil {
		return err
	}
	for k, v := range m.p {
		binary.LittleEndian.PutUint64(b, uint64(k))
		if _, err = w.Write(b); err != nil {
			return err
		}
		binary.LittleEndian.PutUint64(b, uint64(v))
		if _, err = w.Write(b); err != nil {
			return err
		}
	}

	return nil
}
